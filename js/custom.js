// jQuery(document).ready(function() {
//     $('#myCarousel').carousel({
//         interval: 2000
//     })

//     $('.carousel .item').each(function() {
//         var next = $(this).next();
//         if (!next.length) {
//             next = $(this).siblings(':first');
//         }
//         next.children(':first-child').clone().appendTo($(this));

//         if (next.next().length > 0) {
//             next.next().children(':first-child').clone().appendTo($(this));
//         } else {
//             $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
//         }
//     });
// });

(function($) {
    "use strict";

    // manual carousel controls
    $('.next').click(function() { $('.carousel').carousel('next'); return false; });
    $('.prev').click(function() { $('.carousel').carousel('prev'); return false; });

})(jQuery);

var slider = document.getElementById("myRange");
var output = document.getElementById("demo");
output.innerHTML = '$' + slider.value;

slider.oninput = function() {
    output.innerHTML = '$' + this.value;
}